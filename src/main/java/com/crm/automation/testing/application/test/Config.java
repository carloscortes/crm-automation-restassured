package com.crm.automation.testing.application.test;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.crm.automation.testing.application.test")
public class Config {
}
