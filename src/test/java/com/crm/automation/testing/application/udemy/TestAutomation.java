package com.crm.automation.testing.application.udemy;

import com.crm.automation.testing.application.udemy.tools.StringGenerator;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class TestAutomation extends AbstractTest{

    @Autowired
    private StringGenerator stringGenerator = new StringGenerator();

    @Test
    public void testAutomation(){
        String value = stringGenerator.next();
        System.out.println("The value is " +value);
    }
}
