package com.crm.automation.testing.application.udemy.commons;

public enum commonType {

    URL_PROYECT("http://localhost:8080");


    private String key;

    commonType(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

}
