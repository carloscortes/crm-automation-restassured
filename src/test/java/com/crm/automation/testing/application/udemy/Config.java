package com.crm.automation.testing.application.udemy;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan("com.crm.automation.testing.application.udemy")
@Import(
        {
                com.crm.automation.testing.application.test.Config.class
        }
)
public class Config {
}
